# citbx4gitlab: CI toolbox for Gitlab

This toolbox can be used to

* run Gitlab-CI jobs outside a runner on your workstation (classical use case)
* write the script part using a modular format (advanced use case)

## Table of contents

* [The project](#the-project)
    * [System side ci-toolbox tool setup](#system-side-ci-toolbox-tool-setup)
    * [System side ci-toolbox tool update](#system-side-ci-toolbox-tool-update)
    * [ci-toolbox tool integration in a project](#ci-toolbox-tool-integration-in-a-project)
    * [ci-toolbox tool upgrade from a project](#ci-toolbox-tool-upgrade-from-a-project)
* [Use case: Standard Gitlab-CI pipeline job](#use-case-standard-gitlab-ci-pipeline-job)
* [Run a specific job locally](#run-a-specific-job-locally)
    * [First solution: using gitlab-runner tool](#first-solution-using-gitlab-runner-tool)
    * [Second solution: Use ci-toolbox command](#second-solution-use-ci-toolbox-command)
* [Write a job or a module script using CI toolbox](#write-a-job-or-a-module-script-using-ci-toolbox)
    * [Write a job](#write-a-job)
    * [Write a module](#write-a-module)
    * [Useful function list](#useful-function-list)
* [Job examples](#job-examples)
    * [job-minimal](#job-minimal)
    * [job-with-before-after-script](#job-with-before-after-script)
    * [job-advanced](#job-advanced)
    * [job-test-services-mysql](#job-test-services-mysql)
    * [job-test-services-postgres](#job-test-services-postgres)

## The project

The ci-toolbox tool can be installed into the system and/or also integrated into a project tree (useful to force the project team to use a specific version)

The ci-toolbox command (`/usr/local/bin/ci-toolbox`) is a wrapper which will execute `tools/ci-toolbox/ci-toolbox.sh` from the project if found here, otherwise, the ci-toolbox wrapper will use this stored into system (`/usr/local/lib/ci-toolbox/ci-toolbox.sh`)

### System side ci-toolbox tool setup

You can perform this setup (where `master` can be changed by a specific version in the URL):
* Manually by cloning this project and execute `tools/ci-toolbox/ci-toolbox.sh setup`
* Or paste the following content into your shell:

```bash
mkdir -p /tmp/ci-toolbox \
    && curl -fSsL https://gitlab.com/ercom/citbx4gitlab/repository/master/archive.tar.bz2 \
    | tar -xj -C /tmp/ci-toolbox \
    && cd /tmp/ci-toolbox/* \
    && ./tools/ci-toolbox/ci-toolbox.sh setup \
    && cd - \
    && rm -rf /tmp/ci-toolbox
```

### System side ci-toolbox tool update

You can update simply with the command `ci-toolbox setup --component ci-tools` or `ci-toolbox setup --component ci-tools <custom_version>`

### ci-toolbox tool integration in a project

Case of integration of ci-toolbox tool into your project:
```
├── .gitlab-ci.yml
├── tools
│   └── ci-toolbox
│       ├── 3rdparty
│       │   └── bashopts.sh
│       ├── env-setup
│       │   ├── common.sh
│       │   ├── gentoo.sh
│       │   └── ubuntu.sh
│       ├── modules
│       │   ├── ccache.sh
│       │   └── example.sh
│       ├── jobs
│       │   └── job-advanced.sh
│       ├── ci-toolbox.properties
│       └── ci-toolbox.sh
```

The minimal element list:

* `.gitlab-ci.yml`: Gitlab-CI pipeline definition
* `ci-toolbox.sh`: tool to run Gitlab-CI job on the workstation or executed via the `ci-toolbox` command
* `bashopts.sh`: dependency required by ci-toolbox.sh

The recommended additional element list:

* `env-setup`: directory containing the workstation environment setup script
* `ci-toolbox.properties`: CI toolbox project properties

The advanced additional element list:

* `jobs`: directory containing job specific functions
* `modules`: directory containing the modules

### ci-toolbox tool upgrade from a project

You can update embedded ci-toolbox tool simply by run `ci-toolbox update` ou `ci-toolbox update <custom_version>`

## Use case: Standard Gitlab-CI pipeline job

The following schema describe the standard execution of a job called "J" in a Gitlab-CI pipeline

![Global use case](doc/GitlabCIPipelineJob.png)

In this case it will be interesting to run a specific job like J in their suitable environment on your local workstation without commit and push.

The aim is to have the exactly the same build environment on the Gitlab runner and the local workstation

![Simple job use case](doc/GitlabCISingleJob.png)

## Run a specific job locally

### First solution: using gitlab-runner tool

To use this solution you have to:

* install gitlab-runner runner tool on the workstation
* `gitlab-runner exec <executor type> <job name>`

In addition you have to:

* Commit the changes to be taken in board by the gitlab-runner
* Add gitlab-runner executor option like `--docker-image`

**NOTE:** `gitlab-runner exe` command is now deprecated since Gitlab 10, and can be removed from next version

### Second solution: Use ci-toolbox command

This tool is able to:

* Start a docker executor with the suitable parameters (image, etc.)
* Run job without having to commit the local changes

Additional advanced ci-toolbox.sh tool functionalities:

* Add workstation specific parameters and actions
* Add ability to run a shell into the suitable job docker environment
* Split job parts in modular format (Job parts, modules, ...)

Usage:

* `ci-toolbox <command> [arguments...]` from anywhere in the project directory, ... or:
* `path/to/ci-toolbox.sh <command> [arguments...]`

The `ci-toolbox` command, installed into the system during the `path/to/ci-toolbox.sh setup`, add the following functionalities:

* This tool can be run anywhere in a project containing the ci-toolbox.sh script
* This tool is like a shortcut to find and run ci-toolbox.sh script without have to specify the absolute/relative path
* This tool add BASH auto completion support

Know limitations:

* Only applicable for docker and shell executor

## Write a job or a module script using CI toolbox

In addition, to run a classical job (like [job-minimal](#job-minimal)), you can use the CI toolbox to write a job into a modular format (Job parts, modules, ...) like the [job-advanced](#job-advanced) job example.

![Hook execution order](doc/HookExecutionOrder.png)

### Write a job

The job must be defined into `jobs/<my job name>.sh`

This job can use modules using `citbx_use "<my module>"`

The job can define the following functions (hooks) - Only applicable to a local workstation:

* `job_define()`: This function can be used to define options for use case on a workstation
* `job_setup()`: This function can be used to perform some action before setup and start the job environment (docker run)

The job can define the following functions (hooks) - applicable to all environments:

* `job_main()`: The job payload
* `job_after()`: This function can be used to perform some action after the job_main execution. This function is called in ALL case (success or error). This function is called with the process return code as the first argument (will be equal to 0 on success)

Example of job script: [tools/ci-toolbox/jobs/job-advanced.sh](tools/ci-toolbox/jobs/job-advanced.sh)

### Write a module

The module must be defined into `modules/<my module name>.sh`

This module can use other modules using `citbx_use "<my other module>"`

The module can define the following functions (hooks) - Only applicable to a local workstation:

* `citbx_module_<my module name>_define()`: This function can be used to define options for use case on a workstation
* `citbx_module_<my module name>_setup()`: This function can be used to perform some action before setup and start the job environment (docker run)

The job can define the following functions (hooks) - applicable to all environments:

* `citbx_module_<my module name>_before()`: This function can be used to perform some action before the job_main execution
* `citbx_module_<my module name>_after()`: This function can be used to perform some action after the job_main execution. This function is called in ALL case (success or error). This function is called with the return code of the last executed process from `job_main` function as the first argument (will be equal to 0 on success)

Example of module script: [tools/ci-toolbox/modules/example.sh](tools/ci-toolbox/modules/example.sh)

### Useful function list

* `citbx_run_ext_job <job name>`: Run an other job
* `citbx_job_list [prefix]`: Get the job list (optionally with the specified prefix)
* `citbx_use <module name>`: Load a module
* `print_critical <message>`: Print an error message and exit (exit code: 1)
* `print_error <message>`: Print an error message
* `print_warning <message>`: Print a warning message
* `print_note <message>`: Print a note message
* `print_info <message>`: Print an in message

## Job examples

### job-minimal

Minimal dockerized job definition example

Job definition:

```yaml
image: ubuntu:16.04

job-minimal:
  stage: build
  script:
    - echo "Hello world"

after_script:
  - echo "job ${CI_JOB_NAME} end"
```

Job run:

![Job minimal](doc/TestCaseJobMinimal.png)

### job-with-before-after-script

Dockerized job definition example using `before_script` and `after_script`

Job definition:

```yaml
after_script:
  - echo "job ${CI_JOB_NAME} end"

job-with-before-after-script:
  stage: build
  before_script:
    - echo "executed before"
  script:
    - echo "script"
      "on several"
    - echo "lines"
    - cat <<< $(echo 'hi!')
  after_script:
    - echo "executed after"
```

### job-advanced

Dockerized job definition example (file [tools/ci-toolbox/jobs/job-advanced.sh](tools/ci-toolbox/jobs/job-advanced.sh)) with options, arguments, additional treatments and external module use (file [tools/ci-toolbox/modules/example.sh](tools/ci-toolbox/modules/example.sh))

Job definition:

```yaml
job-advanced:
  image: ubuntu:16.04
  stage: build
  variables:
    JOBADVAR: "${CI_JOB_NAME} JOBADVAR value"
  script:
    - echo ${GLOBALJOBVAR}
    - echo ${JOBADVAR}
    - tools/ci-toolbox/ci-toolbox.sh

after_script:
  - echo "job ${CI_JOB_NAME} end"
```

Job run:

![Job advanced](doc/TestCaseJobAvanced.png)

### job-test-services-mysql

Dockerized job definition example with a mysql service

Job definition:

```yaml
job-test-services-mysql:
  stage: build
  variables:
    MYSQL_DATABASE: test
    MYSQL_ROOT_PASSWORD: password
  image: mysql
  services:
    - mysql
  tags:
    - docker
  script:
    - printf "Waiting for mysql";
      while ! mysql -h mysql -u root -ppassword test -e '' > /dev/null 2>&1 ;
      do printf "."; sleep 1; done;
      printf " done!\n"
    - mysql -h mysql -u root -ppassword test -e 'SHOW VARIABLES LIKE "%version%";'
```

### job-test-services-postgres

Dockerized job definition example with a postgresql service

Job definition:

```yaml
job-test-services-postgres:
  stage: build
  image: postgres:9.4
  services:
    - name: postgres:9.4
      alias: db-postgres
      entrypoint: ["docker-entrypoint.sh"]
      command: ["postgres"]
  tags:
    - docker
  script:
    - printf "Waiting for postgres";
      while ! psql -h db-postgres -U postgres -c '' > /dev/null 2>&1 ;
      do printf "."; sleep 1; done;
      printf " done!\n"
    - psql -h db-postgres -U postgres -c 'select version();'
```
