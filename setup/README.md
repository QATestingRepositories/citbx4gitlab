# Project setup directory

This directory is only used by install/update tools and should not be integrated into any project.

Files:
* `install.sh`: This file is read (with `source` command) during install/update process and should not be executed directly
