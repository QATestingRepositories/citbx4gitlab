
set -e

# Print an error message and exit with error status 1
print_critical() {
    >&2 printf "\033[91m[CRIT] %s\033[0m\n" "$@"
    exit 1
}

# Print a note message
print_note() {
    printf "[NOTE] %s\n" "$@"
}

# Pring an info message
print_info() {
    printf "\033[92m[INFO] %s\033[0m\n" "$@"
}

CITBX_TMPDIR=$(mktemp -d)
install_finish() {
    rm -rf $CITBX_TMPDIR
}
trap install_finish EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGALRM SIGTERM

CI_PROJECT_DIR="$(git rev-parse --show-toplevel 2>/dev/null || true)"
if [ -z "$CI_PROJECT_DIR" ]; then
    print_critical "Unable to find the project root directory"
fi

CITBX_CUR_VERSION_MAJOR=$(find "$CI_PROJECT_DIR" -type f -regex \
    "$CI_PROJECT_DIR"'/tools/\(ci-toolbox\|gitlab-ci\)/\(run\|gitlab-ci\)\.sh' \
    -exec sed -E 's/^CITBX_VERSION=([0-9]+).*$/\1/;t;d' '{}' \;)
if [ "$CITBX_CUR_VERSION_MAJOR" != "2" ] && [ "$CITBX_CUR_VERSION_MAJOR" != "3" ]; then
    print_critical "This tool can't be used with version other than 2 and 3"
fi

# Move ci-toolbox directory
if [ -d "$CI_PROJECT_DIR/tools/gitlab-ci" ]; then
    mv -v $CI_PROJECT_DIR/tools/{gitlab-ci,ci-toolbox}
fi
# Move job directory
if [ -d "$CI_PROJECT_DIR/tools/ci-toolbox/run.d" ]; then
    mv -v $CI_PROJECT_DIR/tools/ci-toolbox/{run.d,jobs}
fi
# Move ci-toolbox tool
if [ -f "$CI_PROJECT_DIR/tools/ci-toolbox/run.sh" ]; then
    mv -v $CI_PROJECT_DIR/tools/ci-toolbox/{run,ci-toolbox}.sh
fi
# Move ci-toolbox properties
if [ -f "$CI_PROJECT_DIR/tools/ci-toolbox/citbx.properties" ]; then
    mv -v $CI_PROJECT_DIR/tools/ci-toolbox/{citbx,ci-toolbox}.properties
fi
# Remove link
if [ -L "$CI_PROJECT_DIR/tools/ci-toolbox/run" ]; then
    rm -v "$CI_PROJECT_DIR/tools/ci-toolbox/run"
fi

# Install the last version
CITBX_ABS_DIR="$CI_PROJECT_DIR/tools/ci-toolbox"
print_note "Downloading archive from gitlab.com..."
curl -fSsL https://gitlab.com/ercom/citbx4gitlab/repository/master/archive.tar.bz2 | tar -C $CITBX_TMPDIR -xj
# Execute setup process from the remote archive
CITBX_SRC_DIR="$(readlink -f $CITBX_TMPDIR/citbx4gitlab-*)"
CITBX_SETUP_TYPE="project"
BASHOPTS_FILE_PATH="$(find $PWD -name bashopts.sh)"
source $CITBX_SRC_DIR/setup/install.sh

print_info "Migration done!"
