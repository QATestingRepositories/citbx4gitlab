
citbx_use "example"

job_define() {
    # You can: Define options:
    bashopts_declare -n RUN_JOB_MINIMAL_BEFORE -l run-min-job \
        -d "Run minimal job before" -s -i -t boolean
    bashopts_declare -n JOB_OPTION_EXPORT -l job-opt-export \
        -d "Local job option accissible from the docker" -s -i -t string -v "my exported value"
    bashopts_declare -n JOB_OPTION_EXIT_CODE -l job-exit-code \
        -d "Job exit code" -i -t number -v "0"
    # Add JOB_OPTION_EXPORT to the export list
    CITBX_ENV_EXPORT_LIST+=(JOB_OPTION_EXPORT JOB_OPTION_EXIT_CODE)
}

# You can: Define a setup hook:
job_setup() {
    print_info "=> This part is executed:" \
            "   * only ouside a gilab runner (e.g.: on the devloper workstation)" \
            "   * on the host (outside the docker container)" \
            "   * before start the suitable docker container"
    print_note "Outside the docker:" \
        "* RUN_JOB_MINIMAL_BEFORE=\"$RUN_JOB_MINIMAL_BEFORE\"" \
        "* JOB_OPTION_EXPORT=\"$JOB_OPTION_EXPORT\""
    if [ "$RUN_JOB_MINIMAL_BEFORE" == "true" ]; then
        citbx_run_ext_job job-minimal
    fi
}

job_main() {
    print_info "This part is executed into the suitable docker container"

    if [[ ! -v RUN_JOB_MINIMAL_BEFORE ]]; then
        print_info     " [ OK ] RUN_JOB_MINIMAL_BEFORE=\"$RUN_JOB_MINIMAL_BEFORE\" (is not defined as expected)"
    else
        print_critical " [ KO ] RUN_JOB_MINIMAL_BEFORE=\"$RUN_JOB_MINIMAL_BEFORE\" (is defined)"
    fi

    if [[ -v JOB_OPTION_EXPORT ]]; then
        print_info     " [ OK ] JOB_OPTION_EXPORT=\"$JOB_OPTION_EXPORT\" (is defined as expected)"
    else
        print_critical " [ KO ] JOB_OPTION_EXPORT=\"$JOB_OPTION_EXPORT\" (is not defined)"
    fi

    exit $JOB_OPTION_EXIT_CODE
}

job_after() {
    if [ $1 -eq 0 ]; then
        print_info "job_finish: OK"
    else
        print_error "job_finish: KO (CODE: $1)"
    fi
}
