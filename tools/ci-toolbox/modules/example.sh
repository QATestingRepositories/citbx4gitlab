
citbx_module_example_define() {
    bashopts_declare -n EXAMPLE_VALUE -l example -t string -d "Example option" -v "my example value"
    citbx_export EXAMPLE_VALUE
}

citbx_module_example_setup() {
    print_note "HOOK example: Job setup (EXAMPLE_VALUE='$EXAMPLE_VALUE')"
}

citbx_module_example_before() {
    print_note "HOOK example: Before the job (EXAMPLE_VALUE='$EXAMPLE_VALUE')"
}

citbx_module_example_after() {
    print_note "HOOK example: After the job (exit with code: $1)"
}
